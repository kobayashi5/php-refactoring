<?php


namespace rental;


class Customer
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Rental[]
     */
    private $rentals = [];

    /**
     * Customer constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param Rental $arg
     */
    public function addRental(Rental $arg)
    {
        array_push($this->rentals, $arg);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function statement()
    {
        $totalAmount = 0;
        $frequentRenterPoint = 0;
        $result = "Rental Record for {$this->getName()}\n";

        foreach ($this->rentals as $each) {
            $thisAmount = 0;

            // 一行ごとに金額を計算
            switch ($each->getMovie()->getPriceCode()) {
                case Movie::REGULAR:
                    $thisAmount += 2;
                    if ($each->getDaysRented() > 2) {
                        $thisAmount += ($each->getDaysRented() - 2) * 1.5;
                    }
                    break;
                case Movie::NEW_RELEASE:
                    $thisAmount += $each->getDaysRented() * 3;
                    break;
                case Movie::CHILDRENS:
                    $thisAmount += 1.5;
                    if ($each->getDaysRented() > 3) {
                        $thisAmount = ($each->getDaysRented() - 3) * 1.5;
                    }
                    break;
            }

            // レンタルポイントを加算
            $frequentRenterPoint++;
            // 新作を二日以上借りた場合はボーナスポイント
            if ($each->getMovie()->getPriceCode() === Movie::NEW_RELEASE && $each->getDaysRented() > 1) {
                $frequentRenterPoint++;
            }

            // この貸し出しに関する数値の表示
            $result .= "\t{$each->getMovie()->getTitle()}\t{$thisAmount}\n";
            $totalAmount += $thisAmount;
        }
        // フッターの部分の追加
        $result .= "Amount owed is {$totalAmount}\n";
        $result .= "You earned {$frequentRenterPoint} frequent renter points";
        return $result;
    }
}